package ru.beavers.kubikrubiks.data;

/**
 * Направления вращения грани
 */
public enum Direction {
    CLOCKWISE,
    COUNTERCLOCKWISE
}
