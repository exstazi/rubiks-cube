package ru.beavers.kubikrubiks;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


public class Logger {

    private static FileOutputStream logStream;

    static {
        File pictures = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File photoFile = new File(pictures, System.currentTimeMillis() + "_log.txt");
        try {
            logStream = new FileOutputStream(photoFile);
        } catch (Exception e) {
            Log.d("dbg", e.getMessage());
        }
    }

    public static void dbg (String message) {
        try {
            logStream.write(message.concat("\n").getBytes());
        } catch (IOException e) {
            Log.d("dbg", e.getMessage());
        }
    }

}
