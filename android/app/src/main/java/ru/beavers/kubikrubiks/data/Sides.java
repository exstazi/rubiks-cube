package ru.beavers.kubikrubiks.data;

/**
 * Грани куба
 */
public enum Sides {

    EMPTY (0x00),
    TOP   (0x01),
    LEFT  (0x02),
    FRONT (0x03),
    RIGHT (0x04),
    BACK  (0x05),
    DOWN  (0x06);

    private final int reference;

    private Sides(int reference) {
        this.reference = reference;
    }

    public int getReference() {
        return reference;
    }
}
