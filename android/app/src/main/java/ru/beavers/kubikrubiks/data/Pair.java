package ru.beavers.kubikrubiks.data;

/**
 * Класс, хранящий пару значений
 */
public class Pair<T, P> {

    private T first;
    private P second;

    public Pair(T first, P second) {
        setFirst(first);
        setSecond(second);
    }

    public T getFirst() {
        return first;
    }

    public void setFirst(T first) {
        this.first = first;
    }

    public P getSecond() {
        return second;
    }

    public void setSecond(P second) {
        this.second = second;
    }

    @Override
    public boolean equals(Object another) {
        if (another == null || another.getClass() != Pair.class) {
            return false;
        }
        Pair anotherPair = (Pair)another;
        return first.equals(anotherPair.first) && second.equals(anotherPair.second);
    }
}
