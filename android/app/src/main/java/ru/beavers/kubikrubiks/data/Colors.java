package ru.beavers.kubikrubiks.data;

/**
 * Используемые цвета
 */
public enum Colors {
    UNDEFINED (0x000000),    // 0
    YELLOW    (0xFFFF00),    // 1
    RED       (0xFF0000),    // 2
    BLUE      (0x00FFFF),    // 3
    WHITE     (0xFFFFFF),    // 4
    GREEN     (0x00FF00),    // 5
    ORANGE    (0xFF8000);    // 6

    private final int reference;

    private Colors(int reference) {
        this.reference = reference;
    }

    public int getReference() {
        return reference;
    }
}
