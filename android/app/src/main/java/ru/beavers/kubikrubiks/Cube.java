package ru.beavers.kubikrubiks;


import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ru.beavers.kubikrubiks.data.Direction;
import ru.beavers.kubikrubiks.data.Pair;
import ru.beavers.kubikrubiks.data.Sides;

public class Cube {

    private final String IP = "192.168.4.1";
    private final int PORT = 8888;

    private Socket socket;
    private ListenerSocket listener;
    private OutputStream out;

    public LinkedList<Pair<Sides,Direction>> rotations;

    private int[] configuration =   { 2,2,2,2,2,2,2,2,2,
                                      5,5,5,5,5,5,5,5,5,
                                      1,1,1,1,1,1,1,1,1,
                                      3,3,3,3,3,3,3,3,3,
                                      4,4,4,4,4,4,4,4,4,
                                      6,6,6,6,6,6,6,6,6 };
    private final int[][] rotates =
            { {18,27,19,28,20,29,27,36,28,37,29,38,36,9,37,10,38,11,9,18,10,19,11,20,0,6,1,3,2,0,3,7,5,1,6,8,7,5,8,2},
              {18,9,19,10,20,11,27,18,28,19,29,20,36,27,37,28,38,29,9,36,10,37,11,38,0,2,1,5,2,8,3,1,5,7,6,0,7,3,8,6},
              {0,44,3,41,6,38,44,45,41,48,38,51,45,18,48,21,51,24,18,0,21,3,24,6,9,15,10,12,11,9,12,16,14,10,15,17,16,14,17,11},
              {},
              {6,17,7,14,8,11,17,47,14,46,11,45,47,27,46,30,45,33,27,6,30,7,33,8,18,24,19,21,20,18,21,25,23,19,24,26,25,23,26,20},
              {},
              {26,53,23,50,20,47,53,36,50,39,47,42,36,8,39,5,42,2,8,26,5,23,2,20,27,33,28,30,29,27,30,34,32,28,33,35,34,32,35,29},
              {},
              {2,35,1,32,0,29,35,51,32,52,29,53,51,9,52,12,53,15,9,2,12,1,15,0,36,42,37,39,38,36,39,43,41,37,42,44,43,41,44,38},
              {},
              {24,15,25,16,26,17,15,42,16,43,17,44,42,33,43,34,44,35,33,24,34,25,35,26,45,51,46,48,47,45,48,52,50,46,51,53,52,50,53,47},
              {}
             };

    Cube() {

    }

    public boolean isConnect()
    {
        return (socket!=null);
    }

    public void connect() {
        if( listener != null )
        {
            listener.interrupt();
            listener = null;
        }
        Thread threadConnect =  new Thread() {
            @Override
            public void run() {
                try {
                    InetAddress ipAddress = InetAddress.getByName( IP );
                    socket = new Socket(ipAddress, PORT );

                    out = socket.getOutputStream();
                    listener = new ListenerSocket(socket.getInputStream());
                    listener.start();
                } catch (Exception ex) {
                    Log.d("dbg", "Failed connect to socket: " + ex.getMessage());
                }

            }

        };
        threadConnect.start();
        try {
            threadConnect.join(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if( socket != null )
        {
            //Log.dbg("Connect to socket!" );
        }
    }

    public void disconnect()
    {
        if( socket != null )
        {
            try {
                socket.close();
                socket = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void rotate( byte mes )
    {
        rotate_configure( mes );

        print_configure();

        byte[] message = {mes, 0x00 };
        try {
            out.write( message );
            out.flush();
            Logger.dbg( "before listere rotate byte");
            listener.wait_answer();
            Logger.dbg( "after listener rotate byte");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void rotate( byte[] mes )
    {
        byte[] message = Arrays.copyOf(mes,mes.length+1);
        message[mes.length] = 0x00;
        for( int i = 0; i < mes.length; ++i )
        {
            rotate_configure( mes[0] );
        }

        try {
            out.write( message );
            out.flush();
            Logger.dbg( "Before listener rotate array byte");
            listener.wait_answer();
            Logger.dbg( "After listener rotate array byte");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void rotate( LinkedList<Pair<Sides,Direction>> rotations )
    {
        byte[] message = new byte[rotations.size()];
        for( int i = 0; i < rotations.size(); ++i )
        {
            message[i] = (byte)( (rotations.get(i).getFirst().ordinal() << 1) | rotations.get(i).getSecond().ordinal() );
        }
        rotate( message );
    }

    private void print_configure()
    {
        String[] sideLetters = new String[] {"+U:", "+L:", "+F:", "+R:", "+B:", "+D:"};
        StringBuilder builder = new StringBuilder("");
        for (int i = 0; i < sideLetters.length; i++) {
            builder.append(sideLetters[i]);
            for (int j = 0; j < 9; j++) {
                builder.append(configuration[i * 9 + j]);
            }
        }
        Logger.dbg( builder.toString() );
    }

    public void solve()
    {
        Logger.dbg( "solve--");
        String[] sideLetters = new String[] {"+U:", "+L:", "+F:", "+R:", "+B:", "+D:"};
        StringBuilder builder = new StringBuilder("cubevariablevalue=");
        for (int i = 0; i < sideLetters.length; i++) {
            builder.append(sideLetters[i]);
            for (int j = 0; j < 9; j++) {
                builder.append(configuration[i * 9 + j]);
            }
        }

        Logger.dbg( builder.toString() );

        try
        {
            byte[] postData = builder.toString().getBytes(Charset.forName("UTF-8"));
            URL url = new URL("http://rubiksolve.com/cubesolve.php");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("charset", "utf-8");
            connection.setRequestProperty("Content-Length", Integer.toString(postData.length));
            connection.setUseCaches(false);
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.write(postData);
            wr.flush();

            String inputLine;
            StringBuilder response = new StringBuilder();

            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine).append("\n");
            }

            Pattern pattern = Pattern.compile("Turn The (\\w+) Face (\\w+ )*1.(\\d) Turn");
            Matcher matcher = pattern.matcher(response);
            Logger.dbg( "before create solve.txt");
            File pictures = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            File solveCube = new File(pictures, "solve.txt");
            Logger.dbg( "after create solve.txt" );
            rotations = new LinkedList<>();

            HashMap<String, Sides> sides = new HashMap<>(6);
            sides.put("RED", Sides.TOP);
            sides.put("GREEN", Sides.LEFT);
            sides.put("YELLOW", Sides.FRONT);
            sides.put("BLUE", Sides.RIGHT);
            sides.put("WHITE", Sides.BACK);
            sides.put("ORANGE", Sides.DOWN);

            FileOutputStream outputStream = new FileOutputStream(solveCube);
            outputStream.write((builder.toString() + "\n").getBytes());
            while (matcher.find()){
                 for (int i = 1; i < matcher.groupCount() + 1; ++i) {
                     if (matcher.group(i) != null) {
                         outputStream.write(matcher.group(i).getBytes());
                         outputStream.write(" ".getBytes());

                     }
                 }
                 outputStream.write("\n".getBytes());

                 Sides side = sides.get(matcher.group(1));
                 if (matcher.group(2) == null) {
                     rotations.add(new Pair<>(side, Direction.COUNTERCLOCKWISE));
                     rotations.add(new Pair<>(side, Direction.COUNTERCLOCKWISE));
                 } else {
                     String direction = matcher.group(2).toUpperCase();
                     rotations.add(new Pair<>(side, Direction.valueOf(direction.substring(0, direction.length() - 1))));
                 }
            }
            outputStream.flush();
            outputStream.close();

        }
        catch( Exception e )
        {
            Logger.dbg( "Fuck my brain " + e.toString() );

        }

    }

    private void rotate_configure( byte who )
    {
        int[] copy = Arrays.copyOf( configuration,configuration.length );
        int number = ( ( who >> 1 ) - 1 ) * 2;
        for( int i = 0; i < rotates[number].length; i += 2 )
        {
            configuration[rotates[number][i]] = copy[rotates[number][i+1]];
        }
        if( ( who & 0x01 ) == 1 )
        {
            rotate_configure((byte) (who & 0xFE));
            rotate_configure((byte) (who & 0xFE));
        }
    }

    class ListenerSocket extends Thread
    {
        private InputStream in;
        private volatile byte answer;
        private volatile boolean is_new_answer;

        ListenerSocket( InputStream stream )
        {
            this.in = stream;
            is_new_answer = false;
        }

        @Override
        public void run() {
            while( true )
            {
                try {
                    answer = (byte)in.read();
                    is_new_answer = true;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        public byte wait_answer()
        {
            while( !is_new_answer );
            is_new_answer = false;
            return answer;
        }
    }

}
