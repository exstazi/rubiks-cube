package ru.beavers.kubikrubiks;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

import ru.beavers.kubikrubiks.data.Sides;


public class ActivityMain extends Activity implements View.OnClickListener {

    private Cube cube;

    private Button connect;

    private String networkSSID = "ESP8266";
    private String networkPass = "586348635";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);

        cube = new Cube();


        connect = (Button)findViewById(R.id.connect);
        connect.setOnClickListener( this );

        findViewById(R.id.button).setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cube.disconnect();
                off_wifi();
                Logger.dbg( "off wifi");

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                connect.setEnabled(true);
                on_inet();
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Logger.dbg( "before solve");
                cube.solve();
                Logger.dbg( "after solve");

                on_wifi();
                try {
                    Thread.sleep( 2000 );
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                while( !cube.isConnect() )
                {
                    cube.connect();
                    try {
                        Thread.sleep( 100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                Logger.dbg( "connect to socket");

                connect.setEnabled( false );
                if( cube.rotations != null )
                {
                    Log.d( "dbg", "rotations not null" );
                    cube.rotate(cube.rotations);
                }
            }
        });


        findViewById(R.id.right_top).setOnClickListener(new SendDataToSocket((byte) 0x02));
        findViewById(R.id.right_left).setOnClickListener(new SendDataToSocket((byte) 0x04));
        findViewById(R.id.right_front).setOnClickListener(new SendDataToSocket((byte) 0x06));
        findViewById(R.id.right_right).setOnClickListener(new SendDataToSocket((byte) 0x08));
        findViewById(R.id.right_back).setOnClickListener(new SendDataToSocket((byte) 0x0A));
        findViewById(R.id.right_down).setOnClickListener(new SendDataToSocket((byte) 0x0C));

        findViewById(R.id.left_top).setOnClickListener(new SendDataToSocket((byte) 0x03));
        findViewById(R.id.left_left).setOnClickListener(new SendDataToSocket((byte) 0x05));
        findViewById(R.id.left_front).setOnClickListener(new SendDataToSocket((byte) 0x07));
        findViewById(R.id.left_right).setOnClickListener(new SendDataToSocket((byte) 0x09));
        findViewById(R.id.left_back).setOnClickListener(new SendDataToSocket((byte) 0x0B));
        findViewById(R.id.left_down).setOnClickListener(new SendDataToSocket((byte) 0x0D));
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onPause() {
        super.onPause();
        cube.disconnect();
    }

    private void on_wifi()
    {
        final WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        if( !wifiManager.isWifiEnabled() )
        {
            wifiManager.setWifiEnabled(true);
            WifiConfiguration conf = new WifiConfiguration();
            conf.SSID = "\"" + networkSSID + "\"";
            conf.preSharedKey = "\""+ networkPass +"\"";
            wifiManager.addNetwork(conf);

            List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
            for( WifiConfiguration i : list ) {
                if(i.SSID != null && i.SSID.equals("\"" + networkSSID + "\"")) {
                    wifiManager.disconnect();
                    wifiManager.enableNetwork(i.networkId, true);
                    wifiManager.reconnect();

                    break;
                }
            }
        }
    }

    private void off_wifi()
    {
        final WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        if( wifiManager.isWifiEnabled() )
        {
            wifiManager.setWifiEnabled(false);
            WifiConfiguration conf = new WifiConfiguration();
            conf.SSID = "\"" + networkSSID + "\"";
            conf.preSharedKey = "\""+ networkPass +"\"";
            wifiManager.addNetwork(conf);

            List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
            for( WifiConfiguration i : list ) {
                if(i.SSID != null && i.SSID.equals("\"" + networkSSID + "\"")) {
                    wifiManager.disconnect();
                    break;
                }
            }
        }
    }

    private void on_inet()
    {
        setMobileDataEnabled(this,true);
    }

    private void off_inet()
    {
        setMobileDataEnabled(this,false);
    }

    private void setMobileDataEnabled(Context context, boolean enabled) {
        final ConnectivityManager conman = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            final Class conmanClass = Class.forName(conman.getClass().getName());
            final Field iConnectivityManagerField = conmanClass.getDeclaredField("mService");
            iConnectivityManagerField.setAccessible(true);
            final Object iConnectivityManager = iConnectivityManagerField.get(conman);
            final Class iConnectivityManagerClass = Class.forName(iConnectivityManager.getClass().getName());
            final Method setMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
            setMobileDataEnabledMethod.setAccessible(true);
            setMobileDataEnabledMethod.invoke(iConnectivityManager, enabled);
        }catch (Exception e )
        {

        }
    }

    @Override
    public void onClick(View v) {
        on_wifi();
        cube.connect();
        if( cube.isConnect() ) {
            connect.setEnabled( false );
        } else {
            connect.setEnabled( true );
        }
    }

    class SendDataToSocket implements View.OnClickListener {

        private byte mes;

        SendDataToSocket( byte b )
        {
            mes = b;
        }
        @Override
        public void onClick(View v) {
            cube.rotate( mes );
        }
    }

}
