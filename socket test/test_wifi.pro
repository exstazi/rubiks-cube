#-------------------------------------------------
#
# Project created by QtCreator 2015-09-15T23:16:53
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = test_wifi
TEMPLATE = app


SOURCES += main.cpp\
        viewmain.cpp

HEADERS  += viewmain.h

FORMS    += viewmain.ui
