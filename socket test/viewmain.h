#ifndef VIEWMAIN_H
#define VIEWMAIN_H

#include <QWidget>

#include <QTcpSocket>

namespace Ui {
class ViewMain;
}

class ViewMain : public QWidget
{
    Q_OBJECT

public:
    explicit ViewMain(QWidget *parent = 0);
    ~ViewMain();

private:
    Ui::ViewMain *ui;
    QTcpSocket *socket;
private slots:
    void con();
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();
    void on_pushButton_3_clicked();
    void on_pushButton_4_clicked();
    void on_pushButton_5_clicked();
    void on_pushButton_6_clicked();
    void on_pushButton_7_clicked();
    void on_pushButton_8_clicked();
};

#endif // VIEWMAIN_H
