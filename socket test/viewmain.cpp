#include "viewmain.h"
#include "ui_viewmain.h"
#include <QHostAddress>
#include <QDebug>

ViewMain::ViewMain(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ViewMain)
{
    ui->setupUi(this);
    socket = new QTcpSocket(this);
    socket->connectToHost( "192.168.4.1",8888);
    connect( socket,SIGNAL(connected()),this,SLOT(con()) );
}

void ViewMain::con()
{
    qDebug() << "I'm connected.";
}

ViewMain::~ViewMain()
{
    delete ui;
}

void ViewMain::on_pushButton_clicked()
{
    QByteArray arr;
    arr.append( 0b00000010 );
    arr.append( (char)0 );
    socket->write( arr,2 );
}

void ViewMain::on_pushButton_2_clicked()
{
    QByteArray arr;
    arr.append( 0b00000100 );
    arr.append( (char)0 );
    socket->write( arr,2 );
}

void ViewMain::on_pushButton_3_clicked()
{
    QByteArray arr;
    arr.append( 0b00000110 );
    arr.append( (char)0 );
    socket->write( arr,2 );
}

void ViewMain::on_pushButton_4_clicked()
{
    QByteArray arr;
    arr.append( 0b00001000 );
    arr.append( (char)0 );
    socket->write( arr,2 );
}

void ViewMain::on_pushButton_5_clicked()
{
    QByteArray arr;
    arr.append( 0b00001010 );
    arr.append( (char)0 );
    socket->write( arr,2 );
}

void ViewMain::on_pushButton_6_clicked()
{
    QByteArray arr;
    arr.append( 0b00001100 );
    arr.append( (char)0 );
    socket->write( arr,2 );
}

void ViewMain::on_pushButton_7_clicked()
{
    QByteArray arr;
    arr.append( 0b01000110 );
    arr.append( 0b10001010 );
    arr.append( 0b11000010 );
    arr.append( (char)0 );
    socket->write( arr,4 );
}

void ViewMain::on_pushButton_8_clicked()
{
    QByteArray arr;
    arr.append( 0xFF );
    arr.append( (char)0 );
    socket->write( arr,2 );
}
