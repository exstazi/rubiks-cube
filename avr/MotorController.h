﻿#ifndef MOTORCONTROLLER_H_
#define MOTORCONTROLLER_H_

#include "adc.h"

#define DS_PORT PORTB
#define DS_PIN 2
#define ST_CP_PORT PORTB
#define ST_CP_PIN 1
#define SH_CP_PORT PORTB
#define SH_CP_PIN 0

#define DS_low() DS_PORT&=~_BV(DS_PIN)
#define DS_high() DS_PORT|=_BV(DS_PIN)
#define ST_CP_low() ST_CP_PORT&=~_BV(ST_CP_PIN)
#define ST_CP_high() ST_CP_PORT|=_BV(ST_CP_PIN)
#define SH_CP_low() SH_CP_PORT&=~_BV(SH_CP_PIN)
#define SH_CP_high() SH_CP_PORT|=_BV(SH_CP_PIN)

int motor_status = 0;
int arr_motor_status[12] = { 2,1, 128,64, 4,8, 1024,2048, 512,256, 32,16 };
int arr_motor_adc[6] = { 1,2,0,5,4,3 };

void setPinOut()
{
	SH_CP_low();
	ST_CP_low();
	
	for( int i = 0; i < 12; ++i )
	{
		if( motor_status & (1 << i ) )
			DS_high();
		else
			DS_low();		
		SH_CP_high();
		SH_CP_low();
	}
	
	ST_CP_high();
	_delay_ms( 2 );
	ST_CP_low();
}

void motor_reset()
{
	motor_status = 0;
	setPinOut();	
}

void rotateMotor( int who, int aside )
{
	motor_status = arr_motor_status[who*2+aside];
	setPinOut();
	_delay_ms( 80 );
	while( readADC( arr_motor_adc[who] ) > 512 );
	motor_status = arr_motor_status[who*2+(aside+1)%2];
	setPinOut();
	_delay_ms( 1 );
	motor_reset();
}

void rotateMotors( int who1, int aside1, int who2, int aside2 )
{
	motor_status = arr_motor_status[who1*2+aside1] | arr_motor_status[who2*2+aside2];
	setPinOut();
	_delay_ms( 80 );
	int is_end = 0;
	while( is_end < 2 )
	{
		if( readADC( arr_motor_adc[who1]) > 512 )
		{
			++is_end;
			motor_status = arr_motor_status[who2*2+aside2];
			setPinOut();
		}
		if( readADC( arr_motor_adc[who2]) > 512 )
		{
			++is_end;
			motor_status = arr_motor_status[who1*2+aside1];
			setPinOut();
		}
	}
	motor_reset();
}

#endif /* MOTORCONTROLLER_H_ */