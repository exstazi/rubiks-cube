﻿#ifndef ADC_H_
#define ADC_H_

void initADC()
{
	ADMUX = 0x00;
	ADCSRA = 0x87;
}

int readADC( unsigned int input ) {
	ADMUX = input | ( ADMUX & 0xF0 );
	_delay_ms( 10 );
	ADCSRA |= 0x40;
	while ( (ADCSRA & 0x10)==0 );
	return ADCW;
}




#endif /* ADC_H_ */