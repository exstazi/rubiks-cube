﻿#ifndef WIFI_H_
#define WIFI_H_

#include "Uart.h"

char MODE[15] = { 'A','T','+','C','I','P','M','O','D','E','=','0','\r','\n','\0'};
char MUX[14] = { 'A','T','+','C','I','P','M','U','X','=','1','\r','\n','\0' };
char SERVER[22] = { 'A','T','+','C','I','P','S','E','R','V','E','R','=','1',',','8','8','8','8','\r','\n','\0' };
char SEND[14] = {'A','T','+','C','I','P','S','E','N','D','=','0',',','\0' };

void init_wifi()
{
	write_str_uart( MODE );
	_delay_ms( 10 );
	write_str_uart( MUX );
	_delay_ms( 10 );
	write_str_uart( SERVER );
	_delay_ms( 10 );
}

void write_wifi( char message )
{
	write_str_uart( SEND );
	write_uart( '1' );
	write_uart( '\r' );
	write_uart( '\n' );
	_delay_ms( 30 );
	write_uart( message );
	write_uart( '\r' );
	write_uart( '\n' );
	_delay_ms( 30 );
}

#endif /* WIFI_H_ */