#define F_CPU 16000000L

#include <avr/io.h>
#include <util/delay.h> 
#include <avr/interrupt.h>

#include "MotorController.h"
#include "WiFi.h"

#define ON_LED PORTD = 0b00010000;
#define OFF_LED PORTD = 0b00010000;

void initIO()
{
	DDRD = 255;
	PORTD = 0b10010000;
	
	DDRB = 255;
	PORTB = 0b00000000;
	
	DDRC = 0;
	PORTC = 255;
}

void on_led()
{
	for( int i = 0; i < 1000; ++i )
	{
		for( int j = i; j < 1000; ++j )
			PORTD = 0b10010000;
		for( int j = (1000-i); j >= 0; --j )
			PORTD = 0b10010100;
	}
}

#define SIZE_BUFFER 500

char buffer[SIZE_BUFFER];
unsigned int offset = 0;
unsigned char is_new_data = 0;

char command[100];
unsigned int len = 0;

char who_rotate;
char is_need_rotate = 0;
char is_on_led = 0;

ISR( USART_RXC_vect )
{
	buffer[offset++] = UDR;
	if( UDR == 0 )
	{
		is_new_data = 1;
	}
}

void clear_buffer()
{
	offset = 0;
	is_new_data = 0;
}

void processing_data()
{
	if( offset < 10 )
	return ;
	
	while( buffer[--offset] != ':' );
	len = 0;
	while( buffer[++offset] != 0 )
	{
		if( buffer[offset] == 0xFF )
		{
			command[len++] = 0xFF;
		}
		else
		{			
			command[len++] = buffer[offset] >> 4;
			command[len++] = buffer[offset] & 0x0F;
		}
	}
	if( len != 0 )
	{
		is_need_rotate = 1;
	}
}

int main(void)
{
	initIO();
	initADC();
	motor_reset();
	
	init_uart();
	init_wifi();
	_delay_ms( 300 );
	
	

	clear_buffer();
	sei();	
	
	while( 1 )
	{
		if( is_new_data == 1 )
		{
			cli();
			processing_data();
			
			if( is_need_rotate == 1 )
			{
				for( unsigned int i = 0; i < len; ++i )
				{
					if( command[i] == 0xFF )
					{
						if( is_on_led == 0 )
						{
							is_on_led = 1;
							PORTD = 0b10010100;
						} 
						else 
						{
							is_on_led = 0;
							PORTD = 0b10010000;
						}
					}
					else if( command[i]  != 0 )
					{						
						rotateMotor( (command[i] >> 1)-1,command[i] & 0x01 );
						_delay_ms( 10 );
					}
				}
				is_need_rotate = 0;
			}
			
			write_wifi( 1 );
			clear_buffer();
			sei();
		}
		_delay_ms(20);
	}
	
	//on_led();
	//rotateMotors( 1,0,4,1 );

	
	while( 1 );	
}