﻿#ifndef UART_H_
#define UART_H_

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

void init_uart()
{
	UBRRH = 0;
	UBRRL = 104;
	UCSRA = 0b00000000;
	UCSRB = 0b10011000;
	UCSRC = 0b10000110;
}

void write_uart( char value )
{
	while ( !(UCSRA & (1<<UDRE)) );
	UDR = value;
}

void write_str_uart( char *str )
{
	while (*str != '\0' ) {
		write_uart( *str++ );
	}
}


#endif /* UART_H_ */